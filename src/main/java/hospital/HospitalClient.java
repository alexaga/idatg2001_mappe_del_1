package hospital;

import hospital.exception.RemoveException;
import hospital.healthpersonal.doctor.GeneralPractitioner;

/**
 * The Hospital client.
 */
public class HospitalClient {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        //Creating instance of hospital and assigning test data
        Hospital hospital = new Hospital("Akershus");
        HospitalTestData.fillRegisterWithTestData(hospital);

        //Creating patient to be removed
        Patient patient = new Patient("Hans","Hansen","96611");

        //Removing employee in employee list
        try {
        hospital.getDepartments().get(0).remove(hospital.getDepartments().get(0).getEmployees().get(0));
        } catch(RemoveException re){
            System.out.println(re.getMessage());
        }

        //Removing patient not in patient list
        try {
            hospital.getDepartments().get(0).remove(patient);
        } catch(RemoveException re){
            System.out.println(re.getMessage());
            }
        }
    }


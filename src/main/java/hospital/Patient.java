package hospital;

/**
 * The type Patient.
 */
public class Patient extends Person implements Diagnosable {
    private String diagnosis;

    /**
     * Instantiates a new Patient.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);

    }

    /**
     * Gets diagnose.
     *
     * @return the diagnose
     */
    public String getDiagnose() {
        return diagnosis;
    }

    /**
     *  sets diagnose
     *
     * @param diagnosis the diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        return super.toString()+
                "Diagnosis:: " + diagnosis + '\n';
    }
}

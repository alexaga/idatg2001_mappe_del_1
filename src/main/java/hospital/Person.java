package hospital;

/**
 * The type Person.
 */
public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * Gets first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name.
     *
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
        if(firstName != null) {
            this.firstName = firstName;
        } else {throw new IllegalArgumentException("First name can not be null or blank");}
    }

    /**
     * Gets last name.
     *
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets last name.
     *
     * @param lastName the last name
     */
    public void setLastName(String lastName) {
        if(lastName != null) {
            this.lastName = lastName;
        } else {throw new IllegalArgumentException("Last name can not be null or blank");}
    }

    public String getFullNavn() {return firstName+" "+lastName;}

    /**
     * Gets social security number.
     *
     * @return the social security number
     */
    public String getPersonnummer() {
        return socialSecurityNumber;
    }

    /**
     * Sets social security number.
     *
     * @param socialSecurityNumber the social security number
     */
    public void setPersonnummer(String socialSecurityNumber) {
        if(socialSecurityNumber != null || !socialSecurityNumber.isBlank()) {
            this.socialSecurityNumber = socialSecurityNumber;
        } else {throw new IllegalArgumentException("Social security number can not be null or blank");}
    }

    /**
     * Instantiates a new Person.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    protected Person(String firstName, String lastName, String socialSecurityNumber) {
        setFirstName(firstName);
        setLastName(lastName);
        setPersonnummer(socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "firstName:: " + firstName + '\n' +
                "lastName:: " + lastName + '\n' +
                "socialSecurityNumber:: " + socialSecurityNumber + '\n';
    }
}

package hospital;

/**
 * The interface Diagnosable.
 */
public interface Diagnosable {
    /**
     * Sets diagnosis.
     *
     * @param diagnosis the diagnosis
     */
    public void setDiagnosis(String diagnosis);
}

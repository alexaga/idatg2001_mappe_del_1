package hospital.healthpersonal.doctor;
import hospital.Patient;

/**
 * The type General practitioner.
 */
public class GeneralPractitioner extends Doctor {

    /**
     * Instantiates a new General practitioner.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}

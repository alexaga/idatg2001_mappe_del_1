package hospital.healthpersonal.doctor;
import hospital.Employee;
import hospital.Patient;

/**
 * The type Doctor.
 */
public abstract class Doctor extends Employee {

    /**
     * Instantiates a new Doctor.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    public Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Sets diagnosis.
     *
     * @param patient   the patient
     * @param diagnosis the diagnosis
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}

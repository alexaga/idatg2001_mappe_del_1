package hospital;

import java.util.ArrayList;

/**
 * The type Hospital.
 */
public class Hospital {
    private final String hospitalName;
    private ArrayList<Department> departmentList;

    /**
     * Instantiates a new Hospital.
     *
     * @param hospitalName the hospital name
     */
    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        departmentList = new ArrayList<>();
    }

    /**
     * Gets hospital name.
     *
     * @return the hospital name
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * Gets departments.
     *
     * @return the departments
     */
    public ArrayList<Department> getDepartments() {
        return departmentList;
    }

    /**
     * Add department.
     *
     * @param department the department
     */
    public void addDepartment(Department department) {
        this.departmentList.add(department);
    }

    @Override
    public String toString() {
        return "Hospital Name:: " + hospitalName + '\n' +
                "Departments:: " + departmentList+ "\n";
    }
}

package hospital;

import hospital.exception.RemoveException;

import java.util.ArrayList;

/**
 * The type Department.
 */
public class Department {
    private String departmentName;
    private ArrayList<Employee> employeeList;
    private ArrayList<Patient> patientList;

    /**
     * Instantiates a new Department.
     *
     * @param departmentName the department name
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
        this.employeeList = new ArrayList<>();
        this.patientList = new ArrayList<>();
    }

    /**
     * Gets department name.
     *
     * @return the department name
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Sets department name.
     *
     * @param departmentName the department name
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Gets employees.
     *
     * @return the employees
     */
    public ArrayList<Employee> getEmployees() { return this.employeeList; }

    /**
     * Add employee.
     *
     * @param employee the employee
     */
    public void addEmployee(Employee employee) {
        this.employeeList.add(employee);
    }

    /**
     * Gets patients.
     *
     * @return the patients
     */
    public ArrayList<Patient> getPatients() {
        return this.patientList;
    }

    /**
     * Add patient.
     *
     * @param patient the patient
     */
    public void addPatient(Patient patient) {
        this.patientList.add(patient);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Department that = (Department) o;

        if (!getDepartmentName().equals(that.getDepartmentName())) return false;
        if (!employeeList.equals(that.employeeList)) return false;
        return patientList.equals(that.patientList);
    }

    @Override
    public int hashCode() {
        int result = getDepartmentName().hashCode();
        result = 31 * result + employeeList.hashCode();
        result = 31 * result + patientList.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "name:: " + departmentName + '\n' +
                "list:: " + employeeList +"\n"+
                "list:: " + patientList +"\n";
    }

    /**
     * Method for removing a person from the patient list or employee list.
     *
     * @param person the person
     * @throws RemoveException the exception thrown when an error is expected
     */
    public void remove(Person person) throws RemoveException {
        if (person == null) {
            throw new RemoveException("Person can not be null");
        }
        if (person instanceof Patient) {
            if (patientList.isEmpty()) {
                throw new RemoveException("Patient list is empty");
            }
            else if (patientList.contains(person)) {
                this.patientList.remove(person);
            } else {
                throw new RemoveException("Patient is not in patient list");
            }
        } else if (person instanceof Employee) {
            if (employeeList.isEmpty()) {
                throw new RemoveException("Employee list is empty");
            }
            else if (employeeList.contains(person)) {
                this.employeeList.remove(person);
            } else {
            throw new RemoveException("Employee is not in employee list");
        }
    } else {
            throw new RemoveException("Person is not in any list");
        }
    }
}

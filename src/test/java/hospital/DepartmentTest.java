package hospital;

import hospital.exception.RemoveException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DepartmentTest {

    private Department testDepartment;
    private Department emptyDepartment;

    @DisplayName("Creating test values to be used in tests")
    @BeforeEach
    void createTestValues() {
        testDepartment = new Department("Test");
        emptyDepartment = new Department("Empty");

        testDepartment.addEmployee(new Employee("Odd Even", "Primtallet", ""));
        testDepartment.addEmployee(new Employee("Huppasahn", "DelFinito", ""));

        testDepartment.addPatient(new Patient("Inga", "Lykke", ""));
        testDepartment.addPatient(new Patient("Ulrik", "Smål", ""));
    }

    @DisplayName("Removing patient in the department")
    @Test
    void removePatientInList() {
        int patientListSize = testDepartment.getPatients().size()-1;
        try {
            testDepartment.remove(testDepartment.getPatients().get(0));
        } catch(RemoveException re) {
            re.getMessage();
        }
        assertEquals(patientListSize,testDepartment.getPatients().size());
    }

    @DisplayName("Removing employee in the department")
    @Test
    void removeEmployeeInList() {
        int employeeListSize = testDepartment.getEmployees().size()-1;
        try {
            testDepartment.remove(testDepartment.getEmployees().get(0));
        } catch(RemoveException re) {
            re.getMessage();
        }
        assertEquals(employeeListSize,testDepartment.getEmployees().size());
    }

    @DisplayName("Removing employee not in department")
    @Test
    void removeEmployeeNotInList() {
        Employee employee = new Employee("Hans","Henrik","");

        String exceptionMessage = "";
        try {
            testDepartment.remove(employee);
        } catch (RemoveException re) {
            exceptionMessage = re.getMessage();
        }
        assertTrue(exceptionMessage.contains("Employee is not in employee list"));
    }

    @DisplayName("Removing patient not in department")
    @Test
    void removePatientNotInList() {
        Patient patient = new Patient("Hans","Henrik","");

        String exceptionMessage = "";
        try {
            testDepartment.remove(patient);
        } catch (RemoveException re) {
            exceptionMessage = re.getMessage();
        }
        assertTrue(exceptionMessage.contains("Patient is not in patient list"));
    }

    @DisplayName("Removing person with null value")
    @Test
    void removePersonWithNullValue() {
        String exceptionMessage = "";
        try {
            testDepartment.remove(null);
        } catch (RemoveException re) {
            exceptionMessage = re.getMessage();
        }
        assertTrue(exceptionMessage.contains("Person can not be null"));
    }

    @DisplayName("Removing patient from empty patient list")
    @Test
    void removePatientFromEmptyList() {
        Patient patient = new Patient("Hans","Henrik","");

        String exceptionMessage = "";
        try {
            emptyDepartment.remove(patient);
        } catch (RemoveException re) {
            exceptionMessage = re.getMessage();
        }
        assertTrue(exceptionMessage.contains("Patient list is empty"));
    }

    @DisplayName("Removing employee from empty employee list")
    @Test
    void removeEmployeeFromEmptyList() {
        Employee employee = new Employee("Hans","Henrik","");

        String exceptionMessage = "";
        try {
            emptyDepartment.remove(employee);
        } catch (RemoveException re) {
            exceptionMessage = re.getMessage();
        }
        assertTrue(exceptionMessage.contains("Employee list is empty"));
    }

}